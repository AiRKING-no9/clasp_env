claspテンプレート
===
このプロジェクトを元にclasp環境を導入することで手軽にGoogleAppScriptを補完機能付きでエディタでコーディングが可能
## 準備
node: npm v5.7以上がデフォルトインストールされたバージョン  
※ npm ciができれば良し

## つかいかた
* `npm ci`
*  `npm run login`
*  `https://script.google.com/home/usersettings`にログインしてGoogle APPS Script APIの設定を有効化する  
Google Apps Script APIが無効化されていた場合push時にエラーとなる
*  既にプロジェクト作成済みの場合\
`npm run clone -- [スクリプトID]`  
スクリプトIDはブラウザ上のGoogle App Scriptのページで  
[ファイル]-[プロジェクトのプロパティ]-[情報タブ] スクリプトID欄から取得  
cloneできたスクリプトをVSCode等でコーディングすると入力補完が有効となる
* 新規プロジェクト(スプレッドシート等のファイルを選択)  
`npm run create -- [プロジェクト名]`  
※ プロジェクト名は自由入力
* ローカルで編集した内容をリモートプッシュ  
`npm run push`
* リモートの内容をプル  
`npm run pull`
* Google App Scriptをブラウザ上で開く  
`npm run open_gs`
